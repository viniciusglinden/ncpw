
# Ncurses Personal Wiki

WIP

Ncurses based personal wiki, written in C++.

- test: Catch2
- documentations: Doxygen

For now, git serves as a back-end.

## Features

- [ ] git integration
- [ ] configuration file
- [ ] grep sequences
- [ ] text editor integration

## Dependencies

- make
- C++ compiler, recommended: `g++`

### Planned

- [lgit2](https://libgit2.org)

## Compiling \& installing

Simply:

```sh
make && make install
```

## Running

After installing:

```sh
ncpw
```

## License

[Gnu Public License 2](LICENSE.md)
