CC := g++
CCFLAG := -lncurses

TARGET_NAME := ncpw
MAIN_SRC := ncpw.cpp

default:
	$(CC) $(CCFLAG) $(MAIN_SRC) -o $(TARGET_NAME)

clean:
	rm -rf $(TARGET_NAME)
